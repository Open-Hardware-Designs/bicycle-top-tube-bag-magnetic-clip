# Bicycle Top tube bag magnetic clip

![](pictures/DSC_7941.jpg)

It's a 3 piece set to help mount bag on the top tube of a bicycle.

![](pictures/demo2.gif)

## Why make it?

I wanted to be able to quickly remove the bag so I can use it as my wallet etc.
Bag came with many velcro strips but it takes too long to undo them.
These 3D printed pieces are to replace velcro strips for the bags bottom.

![](pictures/Selection_626.png)
![](pictures/Selection_627.png)
![](pictures/Selection_629.png)

I printed two sets to mount around the alarm holder I have on the tube. One has 8 magnets per side while other has 6 magents. It's not very strong as it can fall off easily if touched or knocked down. But that only happens while carrying a bike or getting on/off it. It never fell off while cycling. I still use the velcro that attaches to the front.

Bag has a slot for hanging. 2 pieces are mounted on it and has some magents while other piece is mounted on the tube with a zip tie and also has some magnets.

Before printining measure your frame size and enter values into the spreadsheet.
Depending on the frame size it varies how many magnets you can fit in. Adjust the sketch if you want to have more magnets.

## BOM

![](pictures/DSC_7548.jpg)

8x Magents 8x3
1x zip tie
3x 3D printed pieces
4x M2 x 6 Self-tapping screws

## Assembly

***Measure frame update values in the spreadsheet***

![](pictures/spreadsheet.png)

Update Frame width and height. If the frame changes its shape then measure at the location where clip will be mounted. Front and back.

![](pictures/11.png)

Then zip tie, and magnet size and length. Ensure length and scew clearance are larger than front/back width.

![](pictures/path828-3-3-6-4.png)

Update magnets sketch in both parts: frame_top, magnet_holder.

![](pictures/Selection_634.png)

Slice it
Magnets are embedded with 3D printed pieces. Therefore stop needs to be inserted in the G-code.
In prusa slicer inserting stop it adds the stop at the beginning of the layer so it should be added at the instance where magnets are being covered.

![](pictures/Selection_631.png)


Print and insert magents.. Make sure magnets polarity is right
Finish the prints.
![](pictures/DSC_7148.jpg)

Insert clip.

![](pictures/DSC_7553.jpg)

Screw magnet

![](pictures/DSC_7555.jpg)

Insert zip tie
![](pictures/DSC_7558.jpg)
![](pictures/DSC_7561.jpg)


Mount on the frame

![](pictures/DSC_7940.jpg)

Install the bag

![](pictures/DSC_7943.jpg)
![](pictures/DSC_7946.jpg)
